package com.etprogroup.takeanoffer;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

/**
 * Created by Bronco on 15-07-2017.
 */

public class ActivityPlace extends ActivityRoot {
    private Activity mContext;
    private Class_User _User;
    private Class_Locations _Locations;
    private Class_Places _Places;

    private JSONObject offers = new JSONObject();
    private JSONObject Places = new JSONObject();
    private String Place = "";
    private String PlaceSelect = null;
    private String PlaceSelectAddress = "";
    private JSONObject Address;
    private ImageView imagePlace;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_place;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_place);
        mContext = this;

        //DATA
        Intent data = getIntent();
        PlaceSelect = data.getStringExtra("option");

        //IMAGES
        imagePlace = (ImageView) findViewById(R.id.imagePlace);
        new ClassGeneral().SetImageDefault(imagePlace, R.drawable.services);

        _User = ((App)getApplicationContext()).User();
        _Locations = ((App)getApplicationContext()).Locations();
        Address = _Locations.Address;

        if(Address.length()>0 && Address!=null){

            try {
                Iterator<String> key = Address.keys();
                while (key.hasNext()) {
                    String Key = key.next();
                    PlaceSelectAddress += Address.get(Key)+", ";
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        //PLACES
        _Places = ((App)getApplicationContext()).Places();
        Places = _Places.Places;
        //PlaceSelect = _Places.PlaceSelect;
        if(PlaceSelect!="" && PlaceSelect!=null){
            FillPlace(PlaceSelect);
        }


        //BUTTON BACK
        Button ButtonBack = (Button) findViewById(R.id.ButtonBack);
        ButtonBack.setEnabled(true);
        ButtonBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    public void FillPlace(String id){
        try {
            JSONObject place = Places.getJSONObject(id);

            String URLimagePlace = place.getString("image");
            String URLPlace = new ClassGeneral().CreateURL(mContext,URLimagePlace);
            new ClassImageDownload(mContext, imagePlace).execute(URLPlace);

            TextView PlaceName = (TextView) mContext.findViewById(R.id.PlaceName);
            PlaceName.setText(place.getString("title"));

            TextView PlaceContent = (TextView) mContext.findViewById(R.id.PlaceContent);
            PlaceContent.setText(place.getString("content"));

            TextView Agency = (TextView) findViewById(R.id.TextPlaceAddress);
            Agency.setText(place.getString("address"));

            TextView Phone = (TextView) findViewById(R.id.TextPlacePhone);
            Phone.setText(place.getString("phone"));

            //Offers
            LinearLayout LayoutOffers = (LinearLayout) mContext.findViewById(R.id.LayoutPlaceOffers);
            LayoutOffers.setPadding(0,50,0,50);


            try {
                //offers = new JSONObject(place.getString("offers"));
                offers = place.getJSONObject("offers");
                Iterator<String> iter = offers.keys();
                while(iter.hasNext()) {
                    String key = iter.next();

                    JSONObject offer = offers.getJSONObject(key);
                    String image = offer.getString("image");
                    String title = offer.getString("title");
                    String content = offer.getString("content");

                    final LinearLayout BuilderLayout = new LinearLayout(mContext);
                    BuilderLayout.setOrientation(LinearLayout.VERTICAL);
                    BuilderLayout.setPadding(0,2,0,0);

                    //TITLE
                    final TextView TextTitle = new TextView(mContext);
                    TextTitle.setTextColor(Color.WHITE);
                    TextTitle.setTextSize(20);
                    TextTitle.setText(title);
                    TextTitle.setPadding(50,50,50,50);
                    BuilderLayout.addView(TextTitle);

                    //CONTENT
                    final TextView TextContent = new TextView(mContext);
                    TextContent.setBackgroundColor(ResourcesCompat.getColor(mContext.getResources(), R.color.colorTransparent, null));
                    //TextContent.setBackgroundColor(Color.BLACK);
                    TextContent.setTextColor(Color.WHITE);
                    //TextContent.setVisibility(View.GONE);
                    TextContent.setText(content);
                    TextContent.setText(new ClassGeneral(mContext).fromHtml(content));
                    TextContent.setTextSize(18);
                    TextContent.setPadding(50,20,50,20);
                    BuilderLayout.addView(TextContent);

                    //ADD LAYOUT
                    LayoutOffers.addView(BuilderLayout);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (JSONException e) {
            e.printStackTrace();

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, ActivityMaps.class);
        //startActivity(intent);
    }

}
