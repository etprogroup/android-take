package com.etprogroup.takeanoffer;

import android.app.Application;
import android.support.multidex.MultiDexApplication;

/**
 * Created by Bronco on 30-07-2017.
 */

public class App extends MultiDexApplication {
    public Application mContext;
    public Class_User _User;
    public Class_Locations _Locations;
    public Class_Services _Services;
    public Class_Places _Places;

    public App(){
        mContext = this;
        _User = new Class_User(mContext);
        _Locations = new Class_Locations(mContext);
        _Services = new Class_Services(mContext);
        _Places = new Class_Places(mContext);

    }

    //USERDATA
    public Class_User User(){
        return _User;
    }

    //USERMAPS
    public Class_Locations Locations(){
        return _Locations;
    }

    //SERVICES
    public Class_Services Services(){
        return _Services;
    }

    //Shop
    public Class_Places Places(){
        return _Places;
    }

}
