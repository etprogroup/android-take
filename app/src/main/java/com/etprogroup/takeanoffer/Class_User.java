package com.etprogroup.takeanoffer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

/**
 * Created by Bronco on 30-07-2017.
 */

public class Class_User {
    private Context mContext;
    public String User = "";
    public String Token = "";
    public String Device = "";
    public boolean Check = false;
    public JSONObject Data = new JSONObject();
    public JSONObject Places = new JSONObject();
    public JSONObject Badges = new JSONObject();
    public String Image = "";
    public String ServiceType = "";
    public Calendar ServiceDate = Calendar.getInstance();
    public int ServiceQuantity = 1;
    public double ServiceTotal = 0;

    public Class_User(Context context) {
        mContext = context;
    }

    //USER
    public void SetDataToken(String token){
        Token = token;
    }
    public String GetDataToken(){
        return Token;
    }

    public void SetDataUser(String user){
        User = user;
    }
    public String GetDataUser(){
        return User;
    }

    public void SetDataDevice(String device){
        Device = device;
    }
    public String GetDataDevice(){
        Device = new ServiceFirebaseInstanceIDService().GetToken();
        return Device;
    }

    public void SetDataData(String data){
        try {
            Data = new JSONObject(data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public JSONObject GetDataData(){
        return Data;
    }

    public void SetDataImage(String image){
        Image = image;
    }
    public String GetDataImage(){
        return Image;
    }

    public void SetDataPlaces(String places){
        try {
            Places = new JSONObject(places);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public JSONObject GetDataPlaces(){
        return Places;
    }

    public void SetDataBadges(String badges){
        try {
            Badges = new JSONObject(badges);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void SetDataBadges(Context context, String badges){
        new ClassDataStorage(context, "write", "badges", badges).execute("");
        SetDataBadges(badges);
    }
    public JSONObject GetDataBadges(){
        return Badges;
    }

    //SERVICES
    public void SetDataServiceType(String Type){
        ServiceType = Type;
    }
    public String GetDataServiceType(){
        return ServiceType;
    }

    public Calendar SetDataServiceDate(Calendar Date, Context context){
        ServiceDate = Date;
        Calendar DateActually = Calendar.getInstance();
        if((DateActually.getTimeInMillis()-600000) > Date.getTimeInMillis()){
            ServiceDate = DateActually;
            Runnable Callback = new Runnable() {
                @Override
                public void run() {

                }
            };
            new ClassGeneral(context).AlertDialog(context.getString(R.string.AlertDialogErrorTitle), context.getString(R.string.AlertDialogErrorMessageTime), Callback);
         }
        return ServiceDate;
    }
    public Calendar GetDataServiceDate(){
        return ServiceDate;
    }

    public void SetDataServiceQuantity(int Quatity){
        ServiceQuantity = Quatity;
    }
    public int GetDataServiceQuantity(){
        return ServiceQuantity;
    }

    public void SetDataServiceTotal(double Total){
        ServiceTotal = Total;
    }
    public double GetDataServiceTotal(){
        return ServiceTotal;
    }


    //CHECK
    public void UserCheck(Context context){
        UserCheck(context, null, null);
    }

    public void UserCheck(Context context, Class activitiy){
        UserCheck(context, null, null);
    }

    public void UserCheck(Context context, Class activitiy, String option){
        if(!Check){
            new ClassGeneral(context).ActivityRedirect(ActivityMain.class, activitiy, option);

        }
    }


    //DATA
    public String GetData(String name, String type){
        String data = "";

        if(type.equals("")){
            type="value";
        }

        try {
            JSONObject jdata = Data.getJSONObject(name);
            data = jdata.getString(type);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return data;
    }


    public void LogOut(final Context context) {
        Check = false;
        SetDataToken("");
        SetDataUser("");
        SetDataImage("");
        SetDataData("{}");

        try {
            JSONObject user = new JSONObject();
            user.put("token", "");
            user.put("user", "");
            user.put("data", "{}");
            user.put("places", "{}");

            ClassDataStorage.AsyncResponse Callback = new ClassDataStorage.AsyncResponse(){

                @Override
                public void processFinish(String output){
                    Intent Intent = new Intent(context, ActivityMain.class);
                    Intent.putExtra("option", "back");
                    context.startActivity(Intent);
                }
            };


            new ClassDataStorage(context, "write", "user", user.toString(), Callback).execute("");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
