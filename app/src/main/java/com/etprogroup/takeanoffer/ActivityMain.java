package com.etprogroup.takeanoffer;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Bronco on 17-07-2017.
 */

public class ActivityMain extends ActivityRoot {
    private Activity mContext;
    private Class_User _User;
    private Class_Services _Services;
    private Class_Locations _Locations;

    //PERMISSIONS
    private boolean permissons = false;
    private static final int PERMISSIONS = 0;
    private static final int PERMISSIONS_WRITE_EXTERNAL_STORAGE = 1;
    private static final int PERMISSIONS_ACCESS_LOCATION = 2;

    NotificationCompat.Builder mBuilder;
    int mNotificationId = 001;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;

        //SERVICES
        startService(new Intent(mContext, ServiceLocations.class));

        _User = ((App)getApplicationContext()).User();
        _Services = ((App)getApplicationContext()).Services();
        _Locations = ((App)getApplicationContext()).Locations();

        Button bmap = (Button) findViewById(R.id.ButtonRegister);
        bmap.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent myIntent = new Intent(view.getContext(), ActivityUserLogin.class);
                startActivityForResult(myIntent, 0);
            }

        });

        Button next = (Button) findViewById(R.id.ButtonService);
        next.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent myIntent = new Intent(view.getContext(), ActivityNavigation.class);
                startActivityForResult(myIntent, 0);
            }

        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        MainEject();

    }

    public void MainEject(){
        if(ValidatePermissons()){
            //this.StorageTest();
            //this.UserRestrict();
            this.ServicesRestrict();
        }
    }


    private void StorageTest(){
        Log.d("storagelog","start");

        final JSONObject user = new JSONObject();

        //SET
        try {
            user.put("token", "tokenxxxxxxxx");
            user.put("user", "userxxxxxxxxxxx");
            user.put("data", "dataxxxxxxxxxx");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ClassDataStorage.AsyncResponse CallbackSET = new ClassDataStorage.AsyncResponse(){

            @Override
            public void processFinish(String output){
                Log.d("storagelog","save");
                Log.d("storagelog-save",output);
            }
        };

        new ClassDataStorage(mContext, "write", "test", user.toString(), CallbackSET).execute("");



        //GET
        ClassDataStorage.AsyncResponse CallbackGET = new ClassDataStorage.AsyncResponse(){

            @Override
            public void processFinish(String output){
                Log.d("storagelog","get");
                Log.d("storagelog-get",output);
            }
        };

        new ClassDataStorage(mContext, "read", "test", "", CallbackGET).execute("");
    }


    public void UserRestrict(){

        ClassDataStorage.AsyncResponse Callback = new ClassDataStorage.AsyncResponse(){

            @Override
            public void processFinish(String output){

                try {
                    JSONObject Callback = new JSONObject(output);

                    if(!Callback.isNull("token")){
                        _User.SetDataToken(Callback.getString("token"));
                    }

                    if(!Callback.isNull("user")){
                        _User.SetDataUser(Callback.getString("user"));
                    }

                    if(!Callback.isNull("image")){
                        _User.SetDataImage(Callback.getString("image"));
                    }

                    if(!Callback.isNull("data")){
                        _User.SetDataData(Callback.getString("data"));
                    }

                    if(!Callback.isNull("places")){
                        _User.SetDataPlaces(Callback.getString("places"));
                    }


                    if(_User.Token.equals("") || _User.User.equals("")){
                        Intent Intent = new Intent(mContext, ActivityUserLogin.class);
                        startActivityForResult(Intent, 0);

                    }else{

                        UserServer();
                    }


                }catch(JSONException e) {
                    e.printStackTrace();
                }

            }
        };

        new ClassDataStorage(mContext, "read", "user", "", Callback).execute("");
    }

    public void UserServer(){

        JSONObject data = new JSONObject();
        try {
            data.put("action", "user_token");
            data.put("user", _User.User);
            data.put("token", _User.Token);
            data.put("device", _User.GetDataDevice());

        } catch (JSONException e){
            e.printStackTrace();
        }

        ClassDataServer.AsyncResponse response = new ClassDataServer.AsyncResponse(){

            @Override
            public void processFinish(String output){
                //new ClassGeneral().CreateMenssage(mContext,"SAVE", output);
                try {
                    JSONObject response = new JSONObject(output);
                    String result = response.getString("result");
                    String message = response.getString("message");
                    final String title = response.getString("title");

                    if(result.equals("success")){
                        _User.Check = true;
                        _User.SetDataToken(response.getString("token"));
                        _User.SetDataUser(response.getString("user"));
                        _User.SetDataData(response.getString("data"));
                        _User.SetDataImage(response.getString("image"));
                        //_User.SetDataPlaces(response.getString("places"));
                        _User.SetDataBadges(mContext,response.getString("badges"));
                        //new ClassGeneral().CreateMenssage(mContext,"response", output);

                        JSONObject user = new JSONObject();
                        user.put("token", _User.GetDataToken());
                        user.put("user", _User.GetDataUser());
                        user.put("data", _User.GetDataData());
                        user.put("places", _User.GetDataPlaces());

                        ClassDataStorage.AsyncResponse Callback = new ClassDataStorage.AsyncResponse(){

                            @Override
                            public void processFinish(String output){
                                //new ClassGeneral().CreateMenssage(mContext,"SAVE", output);
                                ActivityRedirect(ActivityMaps.class);
                            }
                        };

                        new ClassDataStorage(mContext, "write", "user", user.toString(), Callback).execute("");



                    }else if(result.equals("validate")){
                        _User.SetDataToken(response.getString("token"));
                        _User.SetDataUser(response.getString("user"));
                        new ClassGeneral().CreateMenssage(mContext,title, message);
                        ActivityRedirect(ActivityUserSigninCode.class);

                    }else{// if(result.equals("error"))
                        _User.Check = false;
                        _User.SetDataToken("");
                        _User.SetDataUser("");
                        _User.SetDataImage("");
                        _User.SetDataData("{}");

                        JSONObject user = new JSONObject();
                        user.put("token", "");
                        user.put("user", "");
                        user.put("data", "{}");
                        user.put("places", "{}");

                        ClassDataStorage.AsyncResponse Callback = new ClassDataStorage.AsyncResponse(){

                            @Override
                            public void processFinish(String output){
                                ActivityRedirect(ActivityUserLogin.class);
                            }
                        };

                        new ClassGeneral().CreateMenssage(mContext,title, message);
                        new ClassDataStorage(mContext, "write", "user", user.toString(), Callback).execute("");
                     }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };

        new ClassDataServer(mContext, data, response).execute("");


    }


    public void ServicesRestrict(){
        _Services.Get_ServerServices(mContext, new Runnable() {
            public void run() {
                UserRestrict();
            }
        });
    }


    public void ActivityRedirect(Class activity){
        Intent data = getIntent();
        String classname = data.getStringExtra("classname");
        String option = data.getStringExtra("option");

        try {
            if(classname!=null){
                if(!classname.equals("")){
                        activity = Class.forName(classname);
                }
            }

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        //Intent
        Intent Intent = new Intent(mContext, activity);
        if(option!=null) {
            Intent.putExtra("option", option);
        }

        if(option!="exit"){
            startActivity(Intent);

        }
    }


    public void onBackPressed(){
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.putExtra("option", "exit");
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }


    //PERMISSIONS
    public boolean ValidatePermissons(){
        if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
/*
                if (ActivityCompat.shouldShowRequestPermissionRationale(ActContext,
                        Manifest.permission.ACCESS_FINE_LOCATION)) {
                }else{

                }
*/
            ActivityCompat.requestPermissions(mContext,
                    new String[]{
                            android.Manifest.permission.ACCESS_FINE_LOCATION,
                            android.Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSIONS);
            return false;
        }

        if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(mContext,
                    new String[]{
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSIONS);
            return false;
        }
        return true;
    }


    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    MainEject();

                } else {
                    new ClassGeneral().CreateMenssage(mContext,mContext.getString(R.string.PermissionsTitle), mContext.getString(R.string.PermissionsMessage));
                    // permission denied, boo! Disable the functionality that depends on this permission.
                }
                return;
            }
        }
    }


}
